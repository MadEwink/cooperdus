extends Trigger

var characters : Array

func _ready():
	characters = []

func shouldActivate():
	for character in characters:
		if (character.position.x == self.position.x && character.position.y == self.position.y):
			return true
	
	return false

