extends Node2D

class_name CharacterController

export var startName : String

var cx : int
var cy : int

var cellWidth : int = 0
var cellHeight : int = 0

var levelManager : LevelManager

func getLevelMap():
	return levelManager.curLevel

func init():
	levelManager = get_parent().get_node("Managers/LevelManager")
	cellWidth = getLevelMap().cell_size.x
	cellHeight = getLevelMap().cell_size.y
	position = getLevelMap().get_node(startName).position
	cx = int(position.x / cellWidth)
	cy = int(position.y / cellHeight)

func _process(_delta):
	position = Vector2(cx*cellWidth, cy*cellHeight)

func move(dir):
	var posX = cx
	var posY = cy
	
	if (dir == "up"):
		posY -= 1
	elif (dir == "down"):
		posY += 1
	elif (dir == "left"):
		posX -= 1
	elif (dir == "right"):
		posX += 1
	
	if (canMove(posX, posY)):
		cx = posX
		cy = posY

func canMove(posX, posY):
	return !getLevelMap().hasWall(posX, posY) && checkObstacles(posX, posY)

func checkObstacles(posX, posY):
	var obstacles = levelManager.curLevel.find_node("Obstacles").get_children()
	for obstacle in obstacles:
		if (obstacle.position.x == posX*cellWidth && obstacle.position.y == posY*cellHeight && !obstacle.canGoThrough()):
			return false
	return true

func destroy():
	get_parent().remove_child(self)
