extends Obstacle

export var initialStateOpen : bool
var isOpen : bool

var sprite : Sprite

func _ready():
	sprite = get_child(0)
	init()

func init():
	set_open(initialStateOpen)

func set_open(open : bool):
	isOpen = open
	if (isOpen):
		sprite.modulate.a = 0.5
	else:
		sprite.modulate.a = 1.0

func set_active(active):
	set_open(active != initialStateOpen)

func canGoThrough():
	return isOpen
