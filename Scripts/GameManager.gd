extends Node

class_name GameManager

export(Resource) var characterA_resource
export(Resource) var characterB_resource

export(NodePath) var levelManager_path
onready var levelManager : LevelManager = get_node(levelManager_path)

export(NodePath) var startMenu_path
onready var startMenu = get_node(startMenu_path)
export(NodePath) var endUI_path
onready var endUI = get_node(endUI_path)

var characterA : CharacterController
var characterB : CharacterController

signal win

func onStartAction():
	createCharacters()
	startMenu.visible = false
	endUI.visible = false
	
func createCharacters():
	characterA = characterA_resource.instance()
	characterB = characterB_resource.instance()
	
	var root = get_parent().get_parent()
	root.add_child(characterA)
	root.add_child(characterB)
	initLevel()

func initLevel():
	characterA.init()
	characterB.init()
	initObstacles(levelManager.curLevel)
	setTriggersTargets(levelManager.curLevel)

func _physics_process(_delta):
	if (levelManager.curLevel != null):
		checkWin()

func checkWin():
	if (levelManager.curLevel.hasEnd(characterA.cx, characterA.cy) && levelManager.curLevel.hasEnd(characterB.cx, characterB.cy)):
		winLevel()

func winLevel():
	if (levelManager.onWinLevel()):
		initLevel()
	else:
		endUI.visible = true
		characterA.destroy()
		characterB.destroy()
		characterA = null
		characterB = null
		emit_signal("win")


func reinitLevel():
	characterA.init()
	characterB.init()
	initObstacles(levelManager.curLevel)

func initObstacles(level):
	var obstacles = level.find_node("Obstacles").get_children()
	for obstacle in obstacles:
		obstacle.init()

func setTriggersTargets(level):
	var triggers = level.find_node("Triggers").get_children()
	for trigger in triggers:
		trigger.characters.append(characterA)
		trigger.characters.append(characterB)
