extends Node

export(NodePath) var gameManager_path
onready var gameManager : GameManager = get_node(gameManager_path)

export(NodePath) var levelManager_path
onready var levelManager : LevelManager = get_node(levelManager_path)

export(NodePath) var audioPlayer_path
onready var audioPlayer : AudioStreamPlayer = get_node(audioPlayer_path)

var targetA : CharacterController
var targetB : CharacterController

var started : bool

func _ready():
	started = false
	gameManager.connect("win", self, "_on_game_win")

func _input(event : InputEvent):
	var pressed = event.is_pressed()
	if (!pressed):
		return
	
	handleUIAction(event)
	
	if (started):
		handleCharacterAction(event)
		
func handleUIAction(event):
	if (!started && event.is_action("start")):
		levelManager.onStartAction()
		gameManager.onStartAction()
		targetA = gameManager.characterA
		targetB = gameManager.characterB
		started = true
	elif (event.is_action("mute")):
		audioPlayer.stream_paused = !audioPlayer.stream_paused
	elif (event.is_action("restart")):
		gameManager.reinitLevel()
		
func handleCharacterAction(event):
	if (event.is_action("move_downA")):
		targetA.move("down")
	elif (event.is_action("move_rightA")):
		targetA.move("right")
	elif (event.is_action("move_leftA")):
		targetA.move("left")
	elif (event.is_action("move_upA")):
		targetA.move("up")
		
	if (event.is_action("move_downB")):
		targetB.move("down")
	elif (event.is_action("move_rightB")):
		targetB.move("right")
	elif (event.is_action("move_leftB")):
		targetB.move("left")
	elif (event.is_action("move_upB")):
		targetB.move("up")

func _on_game_win():
	started = false
