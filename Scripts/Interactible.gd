extends Node

class_name Interactible

enum InterColor {RED, BLUE}

export(InterColor) var color
