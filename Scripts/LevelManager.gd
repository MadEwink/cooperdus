extends Node

class_name LevelManager

export(Array, Resource) var levels

var curLevelId : int
var curLevel

# Called when the node enters the scene tree for the first time.
func onStartAction():
	curLevelId = 0
	createLevel()

func createLevel():
	curLevel = levels[curLevelId].instance()
	
	var root = 	get_parent().get_parent()
	root.add_child(curLevel)

func onWinLevel():
	curLevel.destroy()
	curLevel = null
	
	if (curLevelId < levels.size()-1):
		curLevelId += 1
		createLevel()
		return true
	return false
