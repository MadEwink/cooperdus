extends TileMap

class_name LevelMap

export(NodePath) var camera_path
var camera : Camera2D

var obstacles : Array
var triggers : Array
var activated : Array

func _ready():
	camera = get_node(camera_path)
	var rect = get_used_rect()
	camera.position = Vector2(rect.position.x*cell_size.x, rect.position.y*cell_size.y)
	var projectWidth = ProjectSettings.get("display/window/size/width");
	var projectHeight = ProjectSettings.get("display/window/size/height");
	var ratioX = rect.size.x * cell_size.x / projectWidth
	var ratioY = rect.size.y * cell_size.y / projectHeight
	var ratio = max(ratioX, ratioY)
	camera.zoom = Vector2(ratio, ratio)
	obstacles = find_node("Obstacles").get_children()
	triggers = find_node("Triggers").get_children()
	activated = []
	for color in Interactible.InterColor:
		activated.append(false)
		

func hasWall(x, y):
	return get_cell(x,y) == 0

func hasEnd(x, y):
	return get_cell(x,y) == 1

func destroy():
	camera.zoom = Vector2(1,1);
	get_parent().remove_child(self)

func _physics_process(_delta):
	for i in range(Interactible.InterColor.size()):
		activated[i] = false
	for trigger in triggers:
		activated[trigger.color] = activated[trigger.color] || trigger.shouldActivate()
	for obstacle in obstacles:
		obstacle.set_active(activated[obstacle.color])

